#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void printMtrx(char **mtrx1, int n, int m)
{
	cout << endl << endl;
	for (int i = 1; i < m+1; i++)
	{
		for (int j = 1; j < n+1; j++)
		{
			cout << mtrx1[i][j] << " ";
		}
		cout << endl;
	}	
	cout << endl;
	for (int j = 1; j < n+1; j++)
	{
		cout << "--";
	}
}

int main()
{
	ifstream inputFile("LIFE.IN.txt");
	if (!inputFile.is_open())
	{
		cout << "ERROR. The input file is not open\n\n";
		return -1;
	}


	int m, n, p;
	inputFile >> m >> n >> p;
	char** mtrx1;
	char** mtrx2;
	mtrx1 = new char* [n+2];
	mtrx2 = new char* [n+2];
	for (int i = 0; i < m+2; i++)
	{
		mtrx1[i] = new char[n+2];
		mtrx2[i] = new char[n+2];
	}

	
	
	for (int i = 0; i < m+2 ; i++)
	{
		for (int j = 0; j < n+2; j++)
		{
			
			mtrx1[i][j] = '0';
			mtrx2[i][j] = '0';
		}
	}
	
	for (int i = 1; i < m+1; i++)
	{
		for (int j = 1; j < n+1; j++)
		{
			
			inputFile >> mtrx1[i][j];
		}
	}
    
	inputFile.close();
	
	
	
	
	for (int j = 1; j < n+1; j++)
	{
		cout << "--";
	}
	printMtrx(mtrx1, n , m);
	
	int ndead = 0, nlive = 0;
	for (int i = 0; i < p; i++)
	{
		for (int i = 1; i < m+1; i++)
		{
			for (int j = 1; j < n+1; j++)
			{
				//cout << "i = " << i << " j = " << j << endl;
				if (mtrx1[i][j] == char ('.'))
				{
				if (mtrx1[i-1][j+1] == '*') ndead += 1;
				if (mtrx1[i][j+1] == '*') ndead += 1;
				if (mtrx1[i+1][j+1] == '*') ndead += 1;
				if (mtrx1[i+1][j] == '*') ndead += 1;
				if (mtrx1[i+1][j-1] == '*') ndead += 1;
				if (mtrx1[i][j-1] == '*' ) ndead += 1;
				if (mtrx1[i-1][j-1] == '*') ndead += 1;
				if (mtrx1[i-1][j] == '*') ndead += 1;
				if (ndead == 3) mtrx2[i][j] = '*';
				else  mtrx2[i][j] =  mtrx1[i][j];
				//cout << "ndead = " << ndead << endl;
				ndead = 0;
				}
			
				if (mtrx1[i][j] == char ('*'))
				{
				if (mtrx1[i-1][j+1] == '*') nlive += + 1;
				if (mtrx1[i][j+1] == '*') nlive += + 1;
				if (mtrx1[i+1][j+1] == '*') nlive += + 1;
				if (mtrx1[i+1][j] == '*') nlive += + 1;
				if (mtrx1[i+1][j-1] == '*') nlive += + 1;
				if (mtrx1[i][j-1] == '*' ) nlive += + 1;
				if (mtrx1[i-1][j-1] == '*') nlive += + 1;
				if (mtrx1[i-1][j] == '*') nlive += + 1;
				if (nlive != 3 and nlive != 2) mtrx2[i][j] = '.';
				else  mtrx2[i][j] =  mtrx1[i][j];
				//cout << "nlive = " << nlive << endl;
				nlive = 0;
				}
			}
		}
		
		for (int i = 1; i < m+1; i++)
		{
			for (int j = 1; j < n+1; j++)
			{
				mtrx1[i][j] = mtrx2[i][j];
			}
		}
		printMtrx(mtrx1, n , m);	
	}
	
	
	
	
	ofstream outputFile("LIFE.OUT.txt");
	if (!outputFile.is_open()) 
	{
		cout << "ERROR. The output file was not opened\n\n" << endl;
		return -1;
	}
	

	for (int i = 1; i < m+1; i++)
	{
		for (int j = 1; j < n+1; j++)
		{
			outputFile << mtrx2[i][j];
		}
		outputFile << endl;
	}
	
	cout << endl << "The program was executed successfully";
	outputFile.close();
	return 0;
}

