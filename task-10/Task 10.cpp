﻿
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main() {
	string INPUT_FILE_PATH = R"(CAPITAL.IN.txt)";
	string OUTPUT_FILE_PATH = R"(CAPITAL.OUT.txt)";
	string text;
	
	ifstream inputFile(INPUT_FILE_PATH);
	if (!inputFile.is_open())
	{
		cout << "ERROR. The input file is not open\n\n";
		return -1;
	}

	getline(inputFile, text);
	
	if (text.empty())
	{
		cout << "ERROR. No suitable text found" << endl;
		return -1;
	}
	
	inputFile.close();

	cout << "Initial text:" << endl;
	cout << text << endl << endl;
	

	for (unsigned int i = 1; i < text.size(); i++)
	{
		if (text[i - 1] == ' ')
		{
			text[i] = toupper((char)text[i]);
		}
	}


	ofstream outputFile(OUTPUT_FILE_PATH);
	if (!outputFile.is_open()) 
	{
		cout << "ERROR. The output file was not opened\n\n" << endl;
		return -1;
	}


	cout << "Converted text:" << endl;
	cout << text << endl;
	
	outputFile.close();
	text.clear();
	return 0;
}
