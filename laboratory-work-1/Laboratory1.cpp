﻿#include <iostream>
#include <string>
#include <string.h>
using namespace std;

int main(int argc, char* argv[])
{

	string userRequest = argv[1];

	if (argc < 2)
	{
		cout << endl;
		cout << "Laboratory work № 1" << endl << endl;
		cout << "Usage:" << endl;
		cout << "command [argument]" << endl << endl;
		cout << "To view a list of commands, use -h or --help ";
		cout << endl;
	}
	else
	{
		if (userRequest == "-h" or userRequest == "--help")
		{
			cout << endl << "Commands list:" << endl;
			cout << """-h (--help)"" - to get help" << endl;
			cout << """-t (--table)"" - to get the size table of all C's basic var types" << endl;
			cout << """-x value (--hex value)"" - to get this value in hex format" << endl;
		}
		else if (userRequest == "-x" or userRequest == "--hex")
		{
			cout << endl << hex << uppercase << stoi(argv[2]) << endl;
		}
		else if (userRequest == "-t" or userRequest == "--table")
		{
			cout << endl << "VAR TYPE\t\ SIZE" << endl;
			cout << "int:\t\t" << sizeof(int) << " bytes" << endl;
			cout << "float:\t\t" << sizeof(float) << " bytes" << endl;
			cout << "char:\t\t" << sizeof(char) << " bytes" << endl;
			cout << "wchar_t:\t" << sizeof(wchar_t) << " bytes" << endl;
			cout << "double:\t\t" << sizeof(double) << " bytes" << endl;
			cout << "string:\t\t" << sizeof(string) << " bytes" << endl;
			cout << "char:\t\t" << sizeof(long) << " bytes" << endl;
			cout << "bool:\t\t" << sizeof(bool) << " bytes" << endl << endl;
		}
		else
		{
			cout << endl << "Unknown request" << endl;
		}
	}
	system("pause");
}

x
