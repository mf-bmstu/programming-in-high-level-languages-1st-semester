﻿#include <iostream>
#include <string>
#include <string.h>
using namespace std;

int main(int argc, char* argv[])
{
	if (argv[1] == (string)"-h")
	{
		cout << endl << "Argument list:" << endl;
		cout << """-h (--help)"" - to get help" << endl;
		cout << """-t (--table)"" - to get the size table of all C's basic var types" << endl;
		cout << """-x value (--hex value)"" - to get this value in hex format" << endl;
	}
	else if ((argv[1] == (string)"-x" or argv[1] == (string)"--hex") and (argc >= 3))
	{
		cout << endl << hex << uppercase << stoi(argv[2]) << endl;
	}
	else if (argv[1] == (string)"-t" or argv[1] == (string)"--table")
	{
		cout << endl << "VAR TYPE\t\ SIZE" << endl;
		cout << "int:\t\t" << sizeof(int) << " bytes" << endl;
		cout << "float:\t\t" << sizeof(float) << " bytes" << endl;
		cout << "char:\t\t" << sizeof(char) << " bytes" << endl;
		cout << "wchar_t:\t" << sizeof(wchar_t) << " bytes" << endl;
		cout << "double:\t\t" << sizeof(double) << " bytes" << endl;
		cout << "string:\t\t" << sizeof(string) << " bytes" << endl;
		cout << "char:\t\t" << sizeof(long) << " bytes" << endl;
		cout << "bool:\t\t" << sizeof(bool) << " bytes" << endl << endl;
	}
	else
	{
		cout << endl << "Unknown request" << endl;
	}
	system("pause");
}
