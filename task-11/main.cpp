#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void convertstr(string& str, const char* ch)
{
	for (int i = 0; i < str.size(); i++)
	{
		if (ch[str[i]] != 0 && str[i] >= 'a' && str[i] <= 'z') str[i] = ch[str[i]];
		if (ch[str[i]+32] != 0 && str[i] >= 'A' && str[i] <= 'Z') str[i] = ch[str[i]+32]-32;
	}
	
}
int main() {

	
	char ch[255] = {0};
	ch['b'] = 'z';
	ch['c'] = 'x';
	ch['d'] = 'w';
	ch['f'] = 'v';
	ch['g'] = 't';
	ch['h'] = 's';
	ch['j'] = 'r';
	ch['k'] = 'q';
	ch['l'] = 'p';
	ch['m'] = 'n';
	
	ch['z'] = 'b';
	ch['x'] = 'c';
	ch['w'] = 'd';
	ch['v'] = 'f';
	ch['t'] = 'g';
	ch['s'] = 'h';
	ch['r'] = 'j';
	ch['q'] = 'k';
	ch['p'] = 'l';
	ch['n'] = 'm';
	
	
	ifstream inputFile("CAPITAL.IN.txt");
	if (!inputFile.is_open())
	{
		cout << "ERROR. The input file is not open\n\n";
		return -1;
	}
	
	ofstream outputFile("CAPITAL.OUT.txt");
	if (!outputFile.is_open()) 
	{
		cout << "ERROR. The output file was not opened\n\n" << endl;
		return -1;
	}
	
	string tempStr;
	
	while (getline(inputFile, tempStr))
	{
		cout << tempStr << endl << "---------" << endl;
		convertstr(tempStr, ch);
		outputFile << tempStr << endl;
	}
	

	inputFile.close();
	outputFile.close();

	return 0;
}


