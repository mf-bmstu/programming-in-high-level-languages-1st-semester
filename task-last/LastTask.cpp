﻿#include <iostream>
#include <iomanip>
using namespace std;

void printMtrx(double** mtrx, int n, int m)
{
	cout << endl;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout << setw(5) << left << mtrx[i][j];
		}
		cout << endl;
	}
}

double maxPosMtrx(double** mtrx, int n, int m)
{
	double max = -1;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (mtrx[i][j] > max)
			{
				max = mtrx[i][j];
			}
		}
	}
	return max;
}

double minPosMtrx(double** mtrx, int n, int m)
{
	double min;
	bool flag = true;
	
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (flag and mtrx[i][j] > 0.0)
			{
				min = mtrx[i][j];
				flag = false;
			}
			if (!flag and (mtrx[i][j] < min) and (mtrx[i][j] > 0.0))
			{
				min = mtrx[i][j];
			}
		}
	}
	return min;
}

int main()
{
	int m, n;
	double** mtrx;
	bool check = true;
	cout << "Enter the number of rows: ";
	cin >> m;
	cout << "Enter the number of columns: ";
	cin >> n;
	mtrx = new double* [n];

	for (int i = 0; i < m; i++)
	{
		mtrx[i] = new double[n];
	}

	cout << "Fill the matrix with at least one positive element:" << endl;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout << "Enter the value of the line number " << i + 1 << " and column number " << j + 1 << ": ";
			cin >> mtrx[i][j];
			if (mtrx[i][j] > 0) check = false;
		}
	}

	if (check)
	{
		cout << "ERROR. no positive elements were found";
		return 1;
	}

	cout << "The original matrix";
	printMtrx(mtrx, n, m);

	double amax = maxPosMtrx(mtrx, n, m);
	double amin = minPosMtrx(mtrx, n, m);
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (mtrx[i][j] == amax)  mtrx[i][j] = amin;
			else if (mtrx[i][j] == amin)  mtrx[i][j] = amax;
		}
	}
	cout << endl << "Replaced the maximum positive element(s) '" << amax << "' and the minimum positive element(s) '" << amin << "'" << endl;
	cout << "The changed matrix:";
	printMtrx(mtrx, n, m);

	for (int i = 0; i < m; i++)
	{
		delete[] mtrx[i];
	}
	delete[] mtrx;
	return 0;
}

