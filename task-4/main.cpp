#include <iostream>
#include <ctime>
using namespace std;

bool primeNumber(int a)
{
		bool res = true;
		if ((a % 2 == 0) || (a % 5 == 0)) return res = false;
		res = true;
		for (int j = 2; j < a; j++)
		{
			if (a % j == 0)
			{
				res = false;
				break;
			}
		}
		return res;
}


int main()
{
	
	int n;
	bool flag;
	cout << "Enter any number from 2 to 10^6" << endl;
	cin >> n;
	
	if (n > 1000000 or n < 2)
	{
		cout << "Error. The number is not within the specified interval";
		return -1;
	}
	cout << "Prime numbers on the interval from " << n << " to " << n*2 << endl;
	
	clock_t start = clock();
	
	for (int i = n; i <= n * 2; i++)
	{
		if (primeNumber(i))
		{
			cout << i << endl;
		} 
	}
	clock_t end = clock();
    cout << "\n----------------" << endl;
    cout << "Time: " << double(end - start) / CLOCKS_PER_SEC << " sec" << endl;
	return 0;
}
	
