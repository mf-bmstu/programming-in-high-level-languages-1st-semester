#include <iostream>
using namespace std;

bool primeNumber(int a)
{
		bool res = true;
		if ((a % 2 == 0) || (a % 5 == 0)) return res = false;
		res = true;
		for (int j = 2; j < a; j++)
		{
			if (a % j == 0)
			{
				res = false;
				break;
			}
		}
		return res;
}

int main()
{
	int N, M, res = 0, A1 = 0 , A2;
	cout << "Enter N (N < 10^9, M > 1, N < M): ";
	cin >> N;
	cout << "Enter M (N < 10^9, M > 1, N < M): ";
	cin >> M;
	if (N >= 1000000000 or M <= 1 or N >= M)
	{
		cout << "Error. The number is not within the specified interval";
		return -1;
	}

	for (int i = N; i <= M; i++)
	{
		if (primeNumber(i)) 
		{
			if (A1 == 0) A1 = i;
			else 
			{
				A2 = i;
				if (A1 == A2 - 2) 
				{
					res = res + 1;
					//cout << A1 << " " << A2 << endl;
				}
				A1 = i;	
			}
		} 	 
	}
	cout << "Count of twin number on segment from N to M: " << res;
}

