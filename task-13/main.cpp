#include <iostream>
#include <fstream>
#include <string>
using namespace std;

/*int perfNumber(int* arr, int size)
{
	ofstream outputFile("DPA.OUT.txt");
	int sum;
	for (int i = 0; i < size; i++)
	{
		for (int j = 1; j < arr[i]; j++)
    	{
    		if (arr[i] % j == 0)
    		{
    			sum += j;
   			}
   		}
   		if (sum < arr[i]) outputFile << "D";
		else if (sum > arr[i]) outputFile << "A";
		else if (sum == arr[i]) outputFile << "P";
		sum = 0;
	}
	outputFile.close();
}*/

char perfNumber(int n)
{
	int sum = 0;
	for (int j = 1; j < n; j++)
    	{
    		if (n % j == 0)
    		{
    			sum += j;
   			}
   		}
   	if (sum < n) return 'D';
	else if (sum > n) return 'A';
	else if (sum == n) return 'P';
}



int main()
{

 	int K, i = 0, N, sum = 0;
 	
	ifstream inputFile("DPA.IN.txt");
	if (!inputFile.is_open())
	{
		cout << "ERROR. The input file is not open\n\n";
		return -1;
	}
	
	inputFile >>  K;
	if (K < 1 || K > 500)
	{
		cout << "ERROR. 'K' is not included in the scope (K >= 1 and K <= 500) \n\n";
		return -1;
	}
	
	int* arr;
	arr = new int[K];

    while (inputFile >> N)
    {
    	if (N > 100000000)
	{
		cout << "ERROR. Element is not included in the scope (n < 10^8) \n\n";
		return -1;
	}
        arr[i++] = N;
    }
    
	inputFile.close();
	
	
	
	fstream outputFile("DPA.OUT.txt");
	if (!outputFile.is_open()) 
	{
		cout << "ERROR. The output file was not opened\n\n" << endl;
		return -1;
	}
	
	for (int i = 0; i < K; i++)
	{
		outputFile << perfNumber(arr[i]);
	}
	
	cout << "The program was executed successfully";
	
	outputFile.close();
	return 0;
}
